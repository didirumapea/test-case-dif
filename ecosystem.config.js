module.exports = {
  apps : [
    {
      name: "test-web-dev",
      script: "npm",
      args: "run dev"
    },
    {
      name: "test-web-prod",
      script: "npm",
      args: "run start"
    },
    {
      name: "test-web-staging",
      script: "npm",
      args: "run start"
    }
  ]
}
