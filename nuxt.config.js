export default {
  ssr: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'test-case-dif',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/axios.js' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  axios: {
    baseURL: process.env.NODE_ENV !== "production"
      ? `http://localhost:5050/api/v1/`
      : "http://128.199.79.229:5050/api/v1/",
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    ['@nuxtjs/axios'],
    '@nuxtjs/auth-next'
  ],

  //   ** The server Property
  // ** https://nuxtjs.org/api/configuration-server
  // */
  server: {
    port: process.env.WEB_PORT, // default: 3000
      host: process.env.NODE_ENV !== "production" ? '0.0.0.0' : '178.128.81.184', // default: localhost,
    // host: '151.106.112.11', // default: localhost,
    // timing: false,
    // https: {
    // 	key: fs.readFileSync(path.resolve(__dirname, '.https/server.key')),
    // 	cert: fs.readFileSync(path.resolve(__dirname, '.https/server.crt'))
    // }
  },
  /*
 ** AUTH configuration
 */
  auth: {
    // Options
    strategies: {
      local: {
        user: {
          autoFetch: false,
          property: 'data'
        },
        endpoints: {
          login: {
            url: 'auth/login',
            method: 'post',
            propertyName: 'token',
          },
          logout: { url: 'cms/auth/admin/logout', method: 'get' },
          user: { url: 'auth/profile', method: 'get', propertyName: '' },
          // user: true,
        },
      },
    },
    redirect: {
      login: '/login',
      logout: '/',
      home: '/',
      callback: '/login',
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
