

export const state = () => ({

});

export const mutations = {

}

export const getters = {
    // Whether the user is currently logged in.
    loggedIn(state) {
        return !!state.currentUser
    },
}

export const actions = {
    // This is automatically run in `src/state/store.js` when the app
    // starts, along with any other actions named `init` in other modules.
    // eslint-disable-next-line no-unused-vars

    async postOrder({ commit }, payload) {
      try {
        return await this.$axios.$post(`order/add`, payload)
      } catch(error) {
        throw error
      }
    },
    async getOrder () {
      try {
        return await this.$axios.$get(`order/list/page=1/limit=100/column-sort=id/sort=asc`)
      } catch(error) {
        throw error
      }
    },

    // Logs in the current user.
    // logIn({ commit, dispatch, getters }, { email, password } = {}) {
    //     if (getters.loggedIn) return dispatch('validate')
    //
    //     return getFirebaseBackend().loginUser(email, password).then((response) => {
    //         const user = response
    //         commit('SET_CURRENT_USER', user)
    //         return Promise.resolve(user)
    //     });
    // },

    // Logs out the current user.
}

// ===
// Private helpers
// ===

function saveState(key, state) {
    if (process.browser) {
        localStorage.setItem(key, JSON.stringify(state))
    }
}
