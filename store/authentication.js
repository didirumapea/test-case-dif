export const state = () => ({
  strategy: 'local'
})
// ACTIONS AS METHODS
export const actions = { // asynchronous
  async nuxtServerInit({ commit }, { req }){
    // commit()
    console.log('server init')
  },
  //region AUTH
  async login ({ commit }, payload) {
    try {
      const response = await this.$axios.$post('auth/login', payload)
      commit('SET_USER', response.data)
    } catch(error) {
      throw error
    }
  },
  //endregion
}

// MUTATION AS LOGIC
export const mutations = { // syncronous
  SET_USER(state, payload){
    this.$auth.setUserToken(payload.token)
    this.$auth.setUser(payload.user)
  },
}
// GETTERS AS DEPLOY RESULT DATA
export const getters = {

}


