export default function ({store, $axios, redirect, app }) {
  // SET GLOBAL HEADER
  $axios.defaults.headers.common = {
    "On-Project": "absenin-x-3-musketeer-2k22",
    "Signature-Key": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm9qZWN0IjoiYWJzZW5pbi1jb2RlLWp3dDJrMjIiLCJpYXQiOjE2NDk2MDQ2NzksImV4cCI6MTY4MTE2MjI3OX0.vQ66U71YcZIqJbk0ax3sPCSB88BqmFXwlJkvU1s34RM",
  };
  $axios.onRequest(config => {
    // if (store.state.auth.user !== null){
    // 	// ADD NEW HEADER
    // 	console.log(app.$auth.loggedIn)
    if (app.$auth.loggedIn) {
      config.headers.common["Authorization"] = app.$auth.strategy.token ? app.$auth.strategy.token.get() : null
    }
    // }
    if (process.env.NODE_ENV === 'development'){
      console.log('SPY: ' + config.url)
      // console.log( process.env.BASE_URL)
    }else {
      // console.log(process.env.API_URL)
    }
  })
}
